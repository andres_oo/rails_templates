#---------------------------------------
if yes?("install basic gems?")
 gem 'semantic-ui-sass', '~> 2.2', '>= 2.2.10.1'
 gem "font-awesome-rails"

 gem_group :development, :test do
  gem "better_errors"
  gem "binding_of_caller"
  gem "figaro"
 end

# into stylesheets
run "rm app/assets/stylesheets/application.css"
file 'app/assets/stylesheets/application.scss', <<-CODE
  @import "semantic-ui";
  @import "font-awesome";
CODE

# into javascript
file 'app/assets/javascripts/application.js', <<-CODE
  //= require jquery
  //= require jquery_ujs
  //= require turbolinks
  //= require semantic-ui
  //= require_tree .
CODE

end

#-----------------------------------------------
if yes?("Use HAML Synthax?")
  gem 'haml-rails', '~> 0.9.0'
  rails_command "generate haml:application_layout convert"
  rails_command "haml:erb2haml"
end

#--------------------------------------------------
if yes?("Would you like to install Devise?")
 gem "devise"
 generate "devise:install"
 model_name = ask("What would you like the user model to be called? [user]")
 model_name = "user" if model_name.blank?
 generate "devise", model_name
end

#----------------------------------------
if yes?("Use Active Admin?")
 gem 'activeadmin', github: 'activeadmin'

 after_bundle do
   generate "active_admin:install"
   rails_command "db:migrate"
   generate "active_admin:resource AdminUser"
   rails_command "db:seed"
 end
end


#------------------------------------------
#------------------------------------------
after_bundle do

  inside('app') do
     run "bundle exec figaro install"
  end

  if yes?("Would you like to use Git?")
   git :init
   git add: "."
   git commit: "-m 'First commit!'"
  end
end
