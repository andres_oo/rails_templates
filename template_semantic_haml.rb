gem 'haml-rails', '~> 0.9.0'
gem 'semantic-ui-sass', '~> 2.2', '>= 2.2.10.1'
gem "font-awesome-rails"
gem 'devise'
gem 'activeadmin', github: 'activeadmin'


gem_group :development, :test do
  gem "better_errors"
  gem "binding_of_caller"
  gem "figaro"
end

rails_command "bundle install"



#haml convert
  #rails_command "generate haml:application_layout convert"
  #rails_command "haml:erb2haml"

# into stylesheets TBD
run "rm app/assets/stylesheets/application.css"
file 'app/assets/stylesheets/application.scss', <<-CODE
  @import "semantic-ui";
  @import "font-awesome";
CODE

# into javascript
file 'app/assets/javascripts/application.js', <<-CODE
  //= require jquery
  //= require jquery_ujs
  //= require turbolinks
  //= require semantic-ui
  //= require_tree .
CODE

# create figaro application.yml
rails_command "bundle exec figaro install"

# Active Admin Setup
generate "devise:install"
generate "active_admin:install"
rails_command "db:migrate"
generate "active_admin:resource AdminUser"
rails_command "db:seed"


git :init
git add: "."
git commit: "-a -m 'Initial commit'"

#if yes?("Would you like to install Devise?")
 # gem "devise"
  #generate "devise:install"
  #model_name = ask("What would you like the user model to be called? [user]")
  #model_name = "user" if model_name.blank?
  #generate "devise", model_name
#end
